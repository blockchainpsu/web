const WebSocket = require('ws')


const wss = new WebSocket.Server({ port: 33217 })

wss.on('connection', ws => {
    ws.on('message', message => {
        console.log('Received message => ${message}')
    })
	var pcsc = require('pcsclite');
	var pcsc = pcsc();

	pcsc.on('reader', function(reader) {

		//CMD = 0x00, 0xA4, 0x04, 0x00, 
		// AID = 0xA0, 0X00, 0x00, 0x00, 0x54, 0x48, 0x00, 0x01
		var cmd_select = new Buffer([0x00, 0xa4, 0x04, 0x00]);
		var cmd_response = new Buffer([0x00, 0xC0, 0x00, 0x00, 0x0D]);
        var cmd_CID = new Buffer([0x80, 0xB0, 0x00, 0x04, 0x02, 0x00, 0x0D]);
        var cmd_Gender = new Buffer([0x80, 0xB0, 0x00, 0xE1, 0x02, 0x00, 0x01]);
						
		var select_with_aid = new Buffer([0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x54, 0x48, 0x00, 0x01]);
		console.log('New reader detected', reader.name);
 
		reader.on('error', function(err) {
			console.log('Error(', this.name, '):', err.message);
		});
 
		reader.on('status', function(status) {
			console.log('Status(', this.name, '):', status);
			/* check what has changed */
			var changes = this.state ^ status.state;
			if (changes) {
				if ((changes & this.SCARD_STATE_EMPTY) && (status.state & this.SCARD_STATE_EMPTY)) {
					console.log("card removed");/* card removed */
					ws.send("Card Removed");
					reader.disconnect(reader.SCARD_LEAVE_CARD, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('Disconnected');
                    }
                });
            } 
			
			else if ((changes & this.SCARD_STATE_PRESENT) && (status.state & this.SCARD_STATE_PRESENT)) {
                console.log("card inserted");/* card inserted */
                reader.connect({ share_mode : this.SCARD_SHARE_SHARED }, function(err, protocol) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('Protocol(', reader.name, '):', protocol);
						
						//Select
						reader.transmit(select_with_aid, 255, protocol, function(err, data) {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log('Data received Select', data);
                                //reader.close();
                                //pcsc.close();
								
								//Get Response
								reader.transmit(cmd_CID, 255, protocol, function(err, data) {
									if (err) {
										console.log(err);
									} else {
										console.log('Data received Response', data);
										reader.transmit(cmd_response, 255, protocol, function(err, data) {
											if (err) {
												console.log(err);
											} 	else {
                                                console.log('Data received CID', data);
                                                data = data.slice(0,data.length-2);
                                                console.log('Data received CID', data);
												resp = data.toString('utf8');
												console.log('Convert Response = ', resp);
												ws.send(resp);
											}
										});

									}
								});
				
                            }
                        });
                    }
                });
            }
        }
    });
 
    reader.on('end', function() {
        console.log('Reader',  this.name, 'removed');
    });
	});
 
	pcsc.on('error', function(err) {
		console.log('PCSC error', err.message);
	});
});