<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Photon &mdash; Colorlib Website Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="gallery.css">
    <script src="../scripts/jquery.min.js"></script>
    <script src="../scripts/bootstrap.min.js"></script>
    <script src="../scripts/sweetalert2.all.min.js"></script>
  </head>
  <body>
      <div class="container">
        <ul class="gallery_box">
            <li>
                <a href="#0">
                    <img src="../images/img_1.jpg">
                    <div class="box_data">
                        <span>Ravi With Bike</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="#0">
                    <img src="../images/img_2.jpg">
                    <div class="box_data">
                        <span>Ravi Singh</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="#0">
                    <img src="../images/img_3.jpg">
                    <div class="box_data">
                        <span>White wall</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="#0">
                    <img src="../images/img_4.jpg">
                    <div class="box_data">
                        <span>Green Tree</span>
                    </div>
                </a>
            </li>
            <!--<li style="position: relative; top: -134px;">-->
            <li>
                <a href="#0">
                    <img src="../images/img_5.jpg">
                    <div class="box_data">
                        <span>Blue</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="#0">
                    <img src="../images/img_6.jpg">
                    <div class="box_data">
                        <span>Ravi</span>
                    </div>
                </a>
            </li>
        </ul>
      </div>
  </body>
</html>