import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {Navbar, Nav,Table} from 'react-bootstrap';

class Header extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      candidate:[["a","candidate A",34],["b","candidate B",98],["c","candidate C",72],["d","candidate D",24]],
    };
  }
    render(){
      return(
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/">Vote Candidate</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="votecdd">ลงคะแนน</Nav.Link>
              <Nav.Link href="result">ผลการเลือกตั้ง</Nav.Link>
            </Nav>
          </Navbar.Collapse>
          <Navbar.Collapse className="justify-content-end">
            <Nav>
              <Nav.Link href="/admin">admin</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      );
    }
}
export default Header;