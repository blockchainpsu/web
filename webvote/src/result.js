import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class Result extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
          show: true,
          backdrop: "static",
          keyboard: false,
          candidate:[["a","candidate A",34],["b","candidate B",98],["c","candidate C",72],["d","candidate D",24]],
        };
      }
    render(){
      return(
        <div>
          <h2>ผลการลงคะแนน</h2>
          <Table striped bordered hover borderless>
              <thead>
                  <tr>
                      <td>หมายเลขผู้สมัคร</td>
                      <td>รายละเอียด</td>
                      <td>ผลคะแนน</td>
                  </tr>
              </thead>
              <tbody>
                  {this.state.candidate.map((value,index)=>(
                      <tr key={index}>
                          {value.map((value2,index2)=>(
                              <td key={index2}>{value2}</td>
                          ))}
                      </tr>
                  ))}
              </tbody>
          </Table>
        </div>
      );
    }
}
export default Result;