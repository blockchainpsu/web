import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Modal,Form,Button,Container,Row,Col } from 'react-bootstrap';
import Fieldset from 'react-fieldset';

class Content extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: true,
      backdrop: "static",
      keyboard: false,
      candidate:["a","b","c","d"],
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }
  render(){
    return(
      <div>
        <Modal show={this.state.show} onHide={this.handleClose} backdrop={this.state.backdrop} keyboard={this.state.keyboard}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        <Container>
          <Form model="user">
            <Fieldset required={true} name="cadidate">
              <Form.Group>
                <Form.Label><h2>เลือกผู้ลงสมัคร</h2></Form.Label>
                <Row>
                  {['radio'].map(type => ( 
                    this.state.candidate.map((value, index) =>(
                      <Col key={index}>
                          <Form.Check 
                            custom
                            type={type} 
                            name={type}
                            id={`${value}`}
                            value={value}
                            label={`candidate ${value}`}
                          />
                      </Col>
                    ))
                  ))}
                </Row>
              </Form.Group>
              <Form.Group>
                <Button type="submit"  variant="outline-success">ลงคะแนน</Button>
              </Form.Group>
            </Fieldset>
          </Form>
        </Container>
      </div>
    );
  }
}
export default Content;