import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header';
import Content from './Content';
import Footer from './Footer';
import Result from './result';
import Admin from './admin';
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter ,Route,browserHistory} from 'react-router-dom';

ReactDOM.render(
  <Header />,document.getElementById("header")
);
ReactDOM.render(
  <BrowserRouter  history={browserHistory}>
    <Route path="/" component={Result}>
      <Route path="/votecdd" component={Content}/>
      <Route path="/result" component={Result}/>
      <Route path="/admin" component={Admin}/>
    </Route>
  </BrowserRouter>,document.getElementById("content")
  
);
ReactDOM.render(
  <Footer />,document.getElementById("footer")
);