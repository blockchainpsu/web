import React, { Component } from 'react';
import { Form,Button,Container,Row,Col } from 'react-bootstrap';
import Fieldset from 'react-fieldset';

class Admin extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            cats: [{id:"", desc:""}],
            owner: "",
            description: ""
        };
      }
      handleChange = (e) => {
        if (["id", "desc"].includes(e.target.className) ) {
          let cats = [...this.state.cats]
          cats[e.target.dataset.id][e.target.className] = e.target.value.toUpperCase()
          this.setState({ cats }, () => console.log(this.state.cats))
        } else {
          this.setState({ [e.target.id]: e.target.value.toUpperCase() })
        }
      }
    addCat = (e) => {
        this.setState((prevState) => ({
          cats: [...prevState.cats, {id:"", desc:""}],
        }));
      }
    handleSubmit = (e) => { e.preventDefault() }
    render(){
      return(
        <div>
          <Container>
            <h4>สร้างการเลือกตั้ง</h4>
              <Form>
                  <Fieldset required={true}>
                      <Form.Group>
                          <Form.Label>ชื่อการโหวต</Form.Label>
                          <Form.Control type="text" id="vname" name="vname" placeholder="ใช่ชื่อการโหวต" />
                      </Form.Group>
                      <Form.Group>
                        <Form.Label>ระยะเวลาการโหวต</Form.Label>
                          <Form.Row>
                            <Col>
                                <Form.Label>เริ่ม</Form.Label>
                                <Form.Control type="date" id="start" name="start" />
                            </Col>
                            <Col>
                                <Form.Label>สินสุด</Form.Label>
                                <Form.Control type="date" id="stop" name="stop" />
                            </Col>
                        </Form.Row>
                      </Form.Group>
                      <Form.Group>
                          <Form.Label>รายชื่อผู้มีสิทธิออกเสียง</Form.Label>
                          <Form.Control type="file" id="list" name="list" placeholder="เลือกรายชื่อไฟล์" />
                          <Form.Check 
                            custom
                            type="checkbox" 
                            name="alllist"
                            id="alllist"
                            value="all"
                            label="ทุกคนมีสิทธิออกเสียง"
                          />
                      </Form.Group>
                      <Form.Group>
                      {
                        this.state.cats.map((val, idx)=> {
                            let catId = `cat-${idx}`, ageId = `age-${idx}`
                            return (
                            <div key={idx}>
                                <Form.Group>
                                    <Form.Row>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>{`ผู้ลงสมัครคนที่ ${idx + 1}`}</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    name={catId}
                                                    data-id={idx}
                                                    id={catId}
                                                    value={this.state.cats[idx].id} 
                                                />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>รายละเอียด</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    name={ageId}
                                                    data-id={idx}
                                                    id={ageId}
                                                    value={this.state.cats[idx].desc} 
                                                />
                                            </Form.Group>
                                        </Col>
                                    </Form.Row>
                                </Form.Group>
                            </div>
                            )
                        })
                        }
                        <Button variant="outline-info" onClick={this.addCat}>เพิ่มผู้ลงสมัคร</Button>
                      </Form.Group>
                      <Form.Group>
                          <Button type="submit" variant="outline-success">สร้างการโหวต</Button>
                      </Form.Group>
                  </Fieldset>
              </Form>
          </Container>
        </div>
      );
    }
}
export default Admin;