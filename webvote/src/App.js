import React, { Component } from 'react';

class App extends Component {
  constructor(){
    super();
  }
  render(){
    return(
      <div>
        <h2><a href="/header">Header</a></h2>
        <h2><a href="/content">Content</a></h2>
        <h2><a href="/footer">Footer</a></h2>
      </div>
    );
  }
}
export default App;
